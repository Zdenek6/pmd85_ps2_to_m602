;=====================================================================================================
;
; Firmware2313.asm
;
; Created: 10.07.2021 23:40:19
; Author : Zdenek
; Web    : maximalne.8u.cz
;
; Firmware pro p�evodn�k PS/2 my� => m602 emulace
;
; Princip �innosti:
; - PS/2 zpr�vy se p�ev�d�j� na emulaci ot��en� clonek
; - PS/2 my� ponech�na ve stavu, kdy odes�l� ka�d�ch 10ms paket se stavem tla��tek a posunem v os�ch X a Y
; - interval 10ms je rozd�len na 129 �sek�, b�hem kter�ch se m�n� v�stupn� hodnoty clonek
; - nejen po�et, ale i d�lka impuls� je �m�rn� rychlosti pohybu
; - bity na portB jsou pop�ehazovan� kv�li snadn�j��mu n�vrhu DPS na konektoru v PMD85
;
;                                        .---__---.         
;                              --- RESET |        | VCC ---- +5V
;                                        |        |           
;                    PS2 - RX  ----- PD0 |        | PB7 ---- DB4
;                                        |        |           
;                             X----- PD1 |        | PB6 ---- DB0 (Mouse Y1)
;                                        |        |           
;                          LED ----- PA1 |        | PB5 ---- DB2 (Mouse X1)
;                                        |        |       
;                             X----- PA0 |        | PB4 ---- DB1 (Mouse Y2)
;                                        |        |       
;                    PS2 - CLK ----- PD2 |        | PB3 ---- DB3 (Mouse X2) 
;                                        |        |       
;                             X----- PD3 |        | PB2 ---- DB7 (Mouse button right)
;                                        |        |         
;                             X----- PD4 |        | PB1 ---- DB6 (Mouse button left)
;                                        |        |            
;                             X----- PD5 |        | PB0 ---- DB5
;                                        |        |            
;                              ----- GND |        | PD6 ---- MODE (jumper)
;                                        `--------'
;
; Fuse L: 0xE4
; Fuse H: 0xDF
; Fuse E: 0xFF
;=====================================================================================================
#define		CPU_FREQ	8000000
;-----------------------------------------------------------------------------------------------------
; Definice pin�
;-----------------------------------------------------------------------------------------------------
#define		LED_ddr		DDRA
#define		LED_port	PORTA
#define		LED_pin		PA1
#define		LED_in		PINA

#define		CLK_ddr		DDRD
#define		CLK_port	PORTD
#define		CLK_pin		PD2
#define		CLK_in		PIND

#define		DATA_ddr	DDRD
#define		DATA_port	PORTD
#define		DATA_pin	PD0
#define		DATA_in		PIND

#define		MODE_ddr	DDRD
#define		MODE_port	PORTD
#define		MODE_pin	PD6

#define		BUS_ddr		DDRB
#define		BUS_port	PORTB

#define		MBL_pin		PB1		; pin indikuj�c� stisk lev�ho tla��tka
#define		MBR_pin		PB2		; pin indikuj�c� stisk prav�ho tla��tka

#define		X1_pin		PB5		 
#define		X2_pin		PB3		 

#define		Y1_pin		PB6
#define		Y2_pin		PB4 
;-----------------------------------------------------------------------------------------------------
; V�znam registr�
;  r2		- uchov�v� SREG b�hem p�eru�en� �asova�e 0
;  r3		- uchov�v� SREG b�hem p�eru�en� uartu
;  r4		- uchov�v� SREG b�hem p�eru�en� �asova�e 1
;  r6		- x_pulse	- kolik impuls� se m� ud�lat v pohybu osy x
;  r7		- x_delta	- p��rustek pro ��ta� x_counter
;  r8		- x_counter - ��ta� pro ur�en� okam�iku zm�ny clonky
;  r9		- y_pulse	- kolik impuls� se m� ud�lat v pohybu osy y
;  r10		- y_delta
;  r11		- y_counter
;  r12      - udr�uje kopii prvn�ho bajtu ze zpr�vy pro ur�en� sm�ru pohybu v ose X a Y a stav tla��tek
;  r13:r14:r15 - 3 bajtov� paket z my�i
;  r19      - konfigurace (rozsv�cen� LED b�hem aktivity my�ky + citlivost pohybu)
;  r20		- posledn� p�ijat� bajt uartem		
;  r21	    - led_counter - po�et bliknut� LED
;  r22      - led_preset  - doba sv�cen� 
;  r23      - led_timer   - odpo�et �asu
;  XL       - ukazatel na hodnotu clonek osy x
;  YL       - ukazatel na hodnotu clonek osy y
;-----------------------------------------------------------------------------------------------------
#define		led_counter		r21
#define		led_preset		r22
#define		led_timer		r23

#define		x_pulse			r6
#define		x_delta			r7	
#define		x_counter		r8
#define		y_pulse			r9
#define		y_delta			r10	
#define		y_counter		r11

#define		config			r19
;-----------------------------------------------------------------------------------------------------
; V�znam bit� v konfiguraci
;-----------------------------------------------------------------------------------------------------
#define CONFIG_LOWRES		0		/* posun my�i je polovi�n� */
#define CONFIG_HIGHRES		1		/* posun my�i je dvojn�sobn� */
#define CONFIG_ACTIVITY		7		/* p�i aktivit� my�ky se rozsv�t� LED */
;-----------------------------------------------------------------------------------------------------
; P��kazy/zpr�vy pro my�
;-----------------------------------------------------------------------------------------------------
#define MOUSE_SET_SCALING_1_1			0xE6
#define MOUSE_SET_SCALING_2_1			0xE7
#define MOUSE_SET_RESOLUTION			0xE8
#define MOUSE_SET_STREAM_MODE			0xEA
#define	MOUSE_ENABLE_PACKET_STREAMING	0xF4
#define MOUSE_SET_DEFAULT				0xF6
#define	MOUSE_ACK						0xFA
#define	MOUSE_RESET						0xFF
;-----------------------------------------------------------------------------------------------------
; V�znam bit� v prvn�m bajtu paket�
; yo xo ys xs ao bm br bl
;-----------------------------------------------------------------------------------------------------
#define MOUSE_YOVF	7	; Y overflow
#define MOUSE_XOVF	6	; X overflow
#define MOUSE_YSGN	5	; znam�nko Y 
#define MOUSE_XSGN	4	; znam�nko X
#define MOUSE_AO	3	; always one pro PS/2
#define MOUSE_MB	2	; middle button
#define MOUSE_RB	1	; right button
#define MOUSE_LB	0	; left button
;-----------------------------------------------------------------------------------------------------
; Ud�losti (bity semafor�) v GPIOR0
;-----------------------------------------------------------------------------------------------------
#define			EVENT_RX	0		; p��jem bajtu z usartu, bajt je v r20	
;=====================================================================================================
; Definice maker
;=====================================================================================================
;-----------------------------------------------------------------------------------------------------
; Macro pro definici re�imu led
; - nap�:		led		5, 10		; => blikne 5x rychlost� 10x10ms 
; - nap�:		led		0			; => vypne LED
;-----------------------------------------------------------------------------------------------------
.macro			led
			.if @0 > 0
				ldi		led_counter, @0
				ldi		led_preset, @1
				rcall	setLED
			.else
				rcall	resetLED
			.endif
.endmacro	
;-----------------------------------------------------------------------------------------------------
; Nastav� PS/2 clock do v�stupn� nuly
;-----------------------------------------------------------------------------------------------------
.macro			clk_0
				sbi		CLK_ddr, CLK_pin	; XCK jako v�stup, hodnota od resetu je nastavena na log. 0
.endmacro	
;-----------------------------------------------------------------------------------------------------
; Nastav� PS/2 clock do log. 1 - t�et� stav podpo�en� p�ipojen�m pull-upem
;-----------------------------------------------------------------------------------------------------
.macro			clk_1
				cbi		CLK_ddr, CLK_pin	; XCK jako vstup, d�ky pullupu se linka nastav� na log. 1
.endmacro	
;-----------------------------------------------------------------------------------------------------
; Nastav� PS/2 data do v�stupn� nuly
;-----------------------------------------------------------------------------------------------------
.macro			data_0
				sbi		DATA_ddr, DATA_pin	; RXD jako v�stup, hodnota od resetu je nastavena na log. 0
.endmacro	
;-----------------------------------------------------------------------------------------------------
; Nastav� PS/2 data do log. 1 - t�et� stav podpo�en� p�ipojen�m pull-upem
;-----------------------------------------------------------------------------------------------------
.macro			data_1
				cbi		DATA_ddr, DATA_pin	; RXD jako vstup, d�ky pullupu se linka nastav� na log. 1
.endmacro	
;-----------------------------------------------------------------------------------------------------
; Zkr�cen� z�pisu konstanty na IO registr
; Nap�.:		outi	SPL, 30
; - �e�� i z�pis na porty nad adresu 0x3f
; - pou��v� registr r16
;-----------------------------------------------------------------------------------------------------
.macro			outi
				ldi		r16, @1
			.if @0 > 0x3f
				sts		@0, r16
			.else
				out		@0, r16
			.endif
.endmacro	
;=====================================================================================================
; SRAM DATA 
;=====================================================================================================				
				.dseg
				.org	0x80
seqx:			.byte	4			; sign�ly pro posun v ose X
seqy:			.byte	4			; sign�ly pro posun v ose y
				.cseg
;=====================================================================================================				
; VEKTORY
;=====================================================================================================				
				; reset
				.org	0
				rjmp	main
				; do�asov�n� �asova�e 1 - generov�n� clonek na datov� sb�rnici
				.org	OC1Aaddr
				rjmp	timer1_ovf
				; p��jem dat p�es UART
				.org	URXCaddr
				rjmp	uart_rx
				; do�asov�n� �asova�e 0 - ��zen� LED
				.org	OC0Aaddr
				rjmp	timer0_ovf
;=====================================================================================================				
; APLIKACE
;=====================================================================================================				
main:			; nastaven� z�sobn�ku - kv�li vol�n� podprogram� a p�eru�en�
				outi	SPL, LOW(RAMEND)
				; na�ten� konfigurace
				rcall	loadconfig
				; nastaven� hardwaru
				rcall	setup
				; zapne watchdog - pokud sel�e inicializace my�i, resetuje se CPU
				rcall	wdt_on
				; b�hem inicializace LED rychle blik�
				led		255, 3
				; pokus o inicializaci my�ky				
				rcall	mouse_init
				; vypne se watch dog
				rcall	wdt_off
				; indikace o poveden� inicializaci - p�lsekundov� rozsv�cen� LED
				led		1, 50
apploop:		; �ek� na na�ten� cel�ho 3 bajtov�ho paketu
				rcall	readPacket		
				; pokud se stisklo prost�edn� tla��tko - spust� se nastaven�
				sbrc	r13, MOUSE_MB
				rjmp	startconfig
				;  indikace aktivity my�i, pokud je povolen� v konfiguraci
				sbrs	config, CONFIG_ACTIVITY
				rjmp	noactivity
				led		1, 5
noactivity:		; update datov� sb�rnice
				rcall	updateData
				; a tak d�le
				rjmp	apploop
; smy�ka pro nastaven� driveru
; - uvoln�n� prost�edn�ho tla��tka - konec nastaven� (ulo�en� do EEPROM)
; - lev� tla��tko - cyklick� zm�na dpi
; - prav� tla��tko - zap/vyp LED indikace
startconfig:	; nastaven� - provizorn� vypnut� LED p�i p��jmu paketu
				eor		x_delta, x_delta	; zastav� pohyb v ose X
				eor		y_delta, y_delta	; zastav� pohyb v ose Y
cfgloop:		rcall	readPacket
				sbrs	r13, MOUSE_MB
				rjmp	cfgexit			; uvoln�n� prost�edn�ho tla��tka => konec nastaven�
				sbrc	r13, MOUSE_RB
				rjmp	cfgRight		; stisk prav�ho tla��tka => zm�na DPI
				sbrc	r13, MOUSE_LB
				rjmp	cfgLeft			; stisk lev�ho tla��tka => zap/vyp indikace aktivity my�i
				rjmp	cfgloop
				; stisk prav�ho tla��tka - zm�na dpi
cfgRight:		led		0
				; rotace dpi v ��slech 01 - 11 - 10 - 01 ...
				mov		r16, config
				andi	r16, (1<<CONFIG_LOWRES) + (1<<CONFIG_HIGHRES)
				ldi		r17, 3
				sbrs	r16, CONFIG_HIGHRES
				dec		r17
				add		r16, r17
				; spojen� nov� konfigurace s configbajtem
				andi	config, (1<<CONFIG_ACTIVITY)
				or		config, r16
				; bliknut� LEDkou 1x/2x/3x podle zvolen� citlivosti
				ldi		led_counter, 2
				sbrc	config, CONFIG_LOWRES
				dec		led_counter
				sbrc	config, CONFIG_HIGHRES
				inc		led_counter
				ldi		led_preset, 15
				rcall	setLED
				rjmp	cfgloop
cfgLeft:		; zm�na bitu FLAG_INDICATION		
				sbrc	config, CONFIG_ACTIVITY
				rjmp	cfgleft1
				; zapnut� indikace
				sbr		config, 1 << CONFIG_ACTIVITY
				led		1, 40
				rjmp	cfgloop
cfgleft1:		; vypnut� indikace
				cbr		config, 1 << CONFIG_ACTIVITY
				led		1, 10
				rjmp	cfgloop
				; konec setupu
cfgexit:		rcall	saveconfig
				rjmp	apploop
	
;-----------------------------------------------------------------------------------------------------
; Podle paketu v r13:r14:r15 nastav� gener�tory pr�b�h� na datov� sb�rnici
;-----------------------------------------------------------------------------------------------------
updateData:		; overflow paket se ignoruje
				sbrc	r13, MOUSE_YOVF
				ret
				sbrc	r13, MOUSE_XOVF
				ret
				; absolutn� hodnoty z offset�
				; X = |X|, Y = |Y|
				sbrc	r13, MOUSE_XSGN
				neg		r14			
				sbrc	r13, MOUSE_YSGN
				neg		r15
				; p�epo�et posun� podle nastaven� citlivosti
				; 3 �rovn�: x0.5; x1; x2
				sbrc	config, CONFIG_HIGHRES
				rjmp	highres
				; polovi�n� dpi
				lsr		r14
				lsr		r15
				rjmp	upd3
highres:		sbrc	config, CONFIG_LOWRES
				rjmp	upd3			; beze zm�ny
				; dvojn�sobn� dpi
				add		r14, r14
				add		r15, r15
upd3:			; nastaven� p��r�stk� pro osy X a Y
				; nastaven� nov�ch parametr�
				cli
				mov		x_delta, r14
				mov		x_pulse, r14
				mov		y_delta, r15
				mov		y_pulse, r15
				ldi		r16, 0xff
				and		x_pulse, r16
				and		y_pulse, r16
				mov		r12, r13
				sei							
				ret
;-----------------------------------------------------------------------------------------------------
; na�ten� konfigura�n�ho bajtu z EEPROM - z adresy 0
;-----------------------------------------------------------------------------------------------------
loadconfig:		outi	EEAR, 0
				sbi		EECR,EERE
				in		config, EEDR
				cpi		config, 0xff
				brne	load2
				ldi		config, CONFIG_ACTIVITY		; v�choz� nastaven� u �ist� EEPROM
load2:			ret
;-----------------------------------------------------------------------------------------------------
; ulo�en� konfigura�n�ho bajtu do EEPROM - na adresu 0
;-----------------------------------------------------------------------------------------------------
saveconfig:		sbic	EECR,EEPE
				rjmp	saveconfig		; �ek� na p��padn� dokon�en� p�edchoz�ho z�pisu				
				out		EEDR, config	; data k z�pisu
				sbi		EECR,EEMPE		; povol� z�pis
				sbi		EECR,EEPE		; spust� z�pis
				ret
;-----------------------------------------------------------------------------------------------------
; Inicializa�n� sekvence my�i
;-----------------------------------------------------------------------------------------------------
mouse_init:		; mal� pauza
				ldi		r16, 20
				rcall	wait
				; ode�le p��kaz RESET
				ldi		r16, MOUSE_RESET
				rcall	sendToDevAck
				brne	mouse_init					
				; �ek� na odpov�� BAT
waitBAT:		rcall	uartGetByte
				cpi		r20, 0xAA
				brne	waitBAT
				; na�te mouseID - mus� b�t 0
				rcall	uartGetByte
				cpi		r20, 0x00
				brne	mouse_init
				; nastav� stream re�im
				ldi		r16, MOUSE_SET_STREAM_MODE
				rcall	sendToDevAck
				brne	mouse_init
				; nastav� scaling - line�rn�
				ldi		r16, MOUSE_SET_SCALING_1_1
				rcall	sendToDevAck
				brne	mouse_init
				; povol� odes�l�n� paket�
				ldi		r16, MOUSE_ENABLE_PACKET_STREAMING
				rcall	sendToDevAck
				brne	mouse_init
				ret
;-----------------------------------------------------------------------------------------------------
; Nastaven� hardwaru
;-----------------------------------------------------------------------------------------------------
setup:			; pullup na pinu s definov�n�m re�imu
				sbi		MODE_port, MODE_pin
				; LED - v�stup
				sbi		LED_ddr, LED_pin
				; datov� sb�rnice je v�stupn�
				outi	BUS_ddr, 0xff
				; nastaven� clonek v SRAM
				ldi		ZH, HIGH(2* seq_table)
				ldi		ZL, LOW(2* seq_table)
				ldi		XH, HIGH(seqx)
				ldi		XL, LOW(seqx)
				ldi		r16, 8
setup_copy:		lpm		r17, Z+
				st		X+, r17
				dec		r16
				brne	setup_copy	
				; X a Y ukazuje na prvn� hodnotu clonek
				ldi		XH, HIGH(seqx)
				ldi		XL, LOW(seqx)
				ldi		YH, HIGH(seqy)
				ldi		YL, LOW(seqy)
				; Timer 1 - vyvol�v� p�eru�en� ka�d�ch 10ms/129
				; Re�im: 0100 => CTC
				; Frekvence: 001 => fcpuclk/1
				outi	TCCR1A, (0<< WGM11) + (0<< WGM10)
				outi	TCCR1B, (0<< WGM13) + (1<< WGM12) + (0<< CS12) + (0<< CS11) + (1<< CS10)
				outi	TCCR1C, 0
				outi	OCR1AH, HIGH(CPU_FREQ/129/100)
				outi	OCR1AL, LOW(CPU_FREQ/129/100)
				outi	TCNT1H, 0x00			; nulov�n� �asova�e
				outi	TCNT1L, 0x00			; nulov�n� �asova�e
				outi	TIMSK, 1<<OCIE1A
				reti
;----------------------------------------------------------------------------------
; P�e�te 3bajtov� paket
; Out: r13:r14:r15
;----------------------------------------------------------------------------------			
readPacket:		rcall	uartGetByte
				sbrs	r20, MOUSE_AO	; 1. bajt - mus� m�t nastaven� bit b3
				rjmp	apploop		
				mov		r13, r20
				rcall	uartGetByte
				mov		r14, r20		
				rcall	uartGetByte
				mov		r15, r20		
				ret
;-----------------------------------------------------------------------------------------------------
; Zapne watchdog - 1s (doba, do kter� se mus� stihnout inicializace my�ky)
;-----------------------------------------------------------------------------------------------------
wdt_on:			wdr						; reset watchdogu p�ed jeho zm�nou
				outi	MCUSR, 0		; vynuluje zdroje resetu
				in		r16, WDTCSR
				ori		r16, (1<<WDCE) | (1<<WDE)
				out		WDTCSR, r16			; spu�t�n� �asov� omezen� zm�ny
				outi	WDTCSR, (1<<WDIF) + (0<<WDIE) + (0<<WDP3) + (1<<WDP2) + (1<<WDP1) + (0<<WDP0) + (1<<WDE)
				ret
;-----------------------------------------------------------------------------------------------------
; Vypne watchdog
;-----------------------------------------------------------------------------------------------------
wdt_off:		wdr
				in		r16, WDTCSR
				ori		r16, (1<<WDCE) | (1<<WDE)
				out		WDTCSR, r16			; spu�t�n� �asov� omezen� zm�ny
				outi	WDTCSR, 0
				ret
;----------------------------------------------------------------------------------
; �ek� na p��jem bajtu
;----------------------------------------------------------------------------------
uartGetByte:	sbis	GPIOR0, EVENT_RX
				rjmp	uartGetByte
				cbi		GPIOR0, EVENT_RX
				ret
;----------------------------------------------------------------------------------
; Ode�le p��kaz do za��zen� a �ek� na ack (0xFA)
;----------------------------------------------------------------------------------
sendToDevAck:	rcall	sendToDevice
				rcall	uartGetByte
				cpi		r20, MOUSE_ACK
				ret
;-----------------------------------------------------------------------------------------------------
; Ode�le p��kaz do p�ipojen�ho za��zen�
; r16 - data k odesl�n�
;-----------------------------------------------------------------------------------------------------
sendToDevice:	; vypne UART, aby negeneroval chyby a p�eru�en�
				rcall	stopReceiving
				; 1. clock do nuly min. na 100 us
				clk_0
				rcall	wait100us
				; 2. data do nuly
				data_0
				rcall	wait100us
				; 3. uvoln� clk
				clk_1
				rcall	wait_clk1		; nutno �ekat, kv�li kapacit�m na veden�
				ldi		r17, 1			; v�po�et parity
				ldi		r18, 8			; po�et odes�lan�ch bit�
				; �ek� na clock od za��zen�
sendloop:		rcall	wait_clk0
				; nastav� datovou linku
				ror		r16
				brcc	send0
				data_1
				inc		r17				; zm�na parity
				rjmp	sendw1
send0:			data_0
sendw1:			; �ek� na vzestupnou hranu
				rcall	wait_clk1
				dec		r18
				brne	sendloop
				; ode�le paritn� bit
				rcall	wait_clk0				
				ror		r17
				brcc	sendp0
				data_1
				rjmp	sendp1
sendp0:			data_0
sendp1:			; �ek� na vzestupnou hranu
				rcall	wait_clk1
				rcall	wait_clk0
				; uvoln�n� datov� linky - stop bit
				data_1
				rcall	wait_data1
				rcall	wait_clk1
				rcall	wait_clk0
				; �ek�, dokud za��zen� nepotvrd� (data = 0)
				rcall	wait_data0
				rcall	wait_clk0
				; �ek� na uvoln�n� obou linek
				rcall	wait_clk1
				rcall	wait_data1
				; hotovo - mo�no zase p�epnout na p��jem
;-----------------------------------------------------------------------------------------------------
; Nastav� UART do re�imu p��jmu
; - 8 bit� + lich� parita + 1 stop bit
; - extern� hodiny (synchronn� re�im)
; - vyvol�v�n� p�eru�en� p�i p��jmu
;-----------------------------------------------------------------------------------------------------
startReceiving:	cli
				push	r16
				rcall	idle_bus
				outi	UCSRA, 0
				outi	UCSRC, (1 << UMSEL) + (1 << UPM1) + (1 << UPM0) + (1 << UCSZ1) + (1 << UCSZ0) + (0 << UCPOL)
				outi	UCSRB, (1 << RXCIE) + (1 << RXEN) + (0 << UCSZ2)
				pop		r16				
				reti
;-----------------------------------------------------------------------------------------------------
; Vypne p��jem dat z UARTu
;-----------------------------------------------------------------------------------------------------
stopReceiving:	cli
				push	r16
				outi	UCSRA, 0
				outi	UCSRB, 0
				outi	UCSRC, 0
				rcall	idle_bus
				pop		r16
				reti
;-----------------------------------------------------------------------------------------------------
; �ek�, dokud clock nen� nula
;-----------------------------------------------------------------------------------------------------
wait_clk0:		sbic	CLK_in, CLK_pin
				rjmp	wait_clk0
				ret
;-----------------------------------------------------------------------------------------------------
; �ek�, dokud clock nen� jedna
;-----------------------------------------------------------------------------------------------------
wait_clk1:		sbis	CLK_in, CLK_pin
				rjmp	wait_clk1
				ret
;-----------------------------------------------------------------------------------------------------
; �ek�, dokud data nen� nula
;-----------------------------------------------------------------------------------------------------
wait_data0:		sbic	DATA_in, DATA_pin
				rjmp	wait_data0
				ret
;-----------------------------------------------------------------------------------------------------
; �ek�, dokud data nen� jedna
;-----------------------------------------------------------------------------------------------------
wait_data1:		sbis	DATA_in, DATA_pin
				rjmp	wait_data1
				ret
;-----------------------------------------------------------------------------------------------------
; �ek� na uklidn�n� s�riov� sb�rnice
;-----------------------------------------------------------------------------------------------------
idle_bus:		; uvoln�n� linek
				data_1
				clk_1
				; �ek� na klidov� stav na komunika�n�ch link�ch
				rcall	wait_clk1
				rcall	wait_data1
				ret
;-----------------------------------------------------------------------------------------------------
; �ek� cca 100 us
;-----------------------------------------------------------------------------------------------------
wait100us:		push	r16
				push	r17
				ldi		r16, CPU_FREQ / 1000000	; po�et takt� na 1 us
w100b:			ldi		r17, 25
w100a:			nop
				dec		r17		
				brne	w100a	; 25 * 4 = 100 takt�
				dec		r16
				brne	w100b	; 20 * 25 * 4
				pop		r17
				pop		r16
				ret
;-----------------------------------------------------------------------------------------------------
; Pauza
; r16 - min. po�et 10 ms, kter� se maj� po�kat
;-----------------------------------------------------------------------------------------------------
wait:			ldi		r17, 100
wait_a:			rcall	wait100us
				dec		r17
				brne	wait_a		; 100 * 0.1 = 10 ms
				dec		r16
				brne	wait
				ret
;-----------------------------------------------------------------------------------------------------
; Vypne LED
;-----------------------------------------------------------------------------------------------------
resetLED:		outi	TCCR0B, 0
				ret
;-----------------------------------------------------------------------------------------------------
; Zapne LED a �asova� timer 0 pro vypnut�
; in: led_counter - po�et bliknut�
;     led_preset  - po�et 10ms, kter� m� LED sv�tit
;-----------------------------------------------------------------------------------------------------
setLED:			sbi		LED_port, LED_pin
				mov		led_timer, led_preset
				; konfigurace �asova�e pro LED status
				; - generov�n� p�eru�en� ka�d�ch 10ms
				; WGM = 010 => CTC
				; OCRA = CPU_FREQ/1024/100
				; CS = 101 = fclk/1024
				outi	TCCR0A, (1<<WGM01) + (0<<WGM00)
				outi	TCCR0B, (0<<WGM02) + (1<<CS02) + (0<<CS01) + (1<<CS00)
				outi	OCR0A, CPU_FREQ/1024/100
				outi	TCNT0, 0
				in		r16, TIMSK
				ori		r16, (1<<OCIE0A)
				out		TIMSK, r16
				ret
;-----------------------------------------------------------------------------------------------------
; Obsluha p�eru�en� po p�ijet� dat z uartu
;-----------------------------------------------------------------------------------------------------
uart_rx:		in		r3, SREG
				push	r16
				; nejprve test chyb
				in		r16, UCSRA
				andi	r16, (1<<FE) + (1<<DOR) + (1<<UPE)
				in		r20, UDR
				brne	uart_rx_exit		; do�lo k chyb�, tento p��jem se bude ignorovat		
				sbi		GPIOR0, EVENT_RX	; nastaven� semaforu - p��jem dat	
uart_rx_exit:	pop		r16
				out		SREG, r3
				reti		
;-----------------------------------------------------------------------------------------------------
; Obsluha p�eru�en� po do��t�n� �asova�e 1
; - generov�n� sign�l� clonek
;-----------------------------------------------------------------------------------------------------
timer1_ovf:		in		r4, SREG	
				push	r16
				push	r17
				; sestav� v�stup pro clonky v ose X a Y
				ld		r16, X
				ld		r17, Y
				or		r16, r17
				sbrc	r12, MOUSE_LB
				sbr		r16, 1 << MBL_pin
				sbrc	r12, MOUSE_RB
				sbr		r16, 1 << MBR_pin
				out		BUS_port, r16
				; update ��ta�e X
				add		x_counter, x_delta
				brcc	t1_skipx
				dec		XL
				sbrs	r12, MOUSE_XSGN		
				adiw	X, 2
				andi	XL, 3
				ori		XL, LOW(seqx)
				dec		x_pulse
				brne	t1_skipx
				eor		x_delta, x_delta	; x_delta = 0 .. dal�� pohyb se u� nekon�
t1_skipx:		; update ��ta�e Y
				add		y_counter, y_delta
				brcc	t1_skipy
				dec		YL
				sbrs	r12, MOUSE_YSGN		
				adiw	Y, 2
				andi	YL, 3
				ori		YL, LOW(seqy)
				dec		y_pulse
				brne	t1_skipy
				eor		y_delta, y_delta	; y_delta = 0 .. dal�� pohyb se u� nekon�
t1_skipy:		pop		r17
				pop		r16							
				out		SREG, r4
				reti
;-----------------------------------------------------------------------------------------------------
; Obsluha p�eru�en� po do��t�n� �asova�e 0
; - ��zen� blik�n� LED
;-----------------------------------------------------------------------------------------------------
timer0_ovf:		in		r2, SREG				
				tst		led_counter
				breq	t0_exit
				; nejprve odpo�et doby svitu
				dec		led_timer
				brne	t0_exit
				mov		led_timer, led_preset	; restart �asova�e
				; zm�na stavu LED
				sbic	LED_port, LED_pin
				rjmp	t0_ledoff
				sbi		LED_port, LED_pin				
				rjmp	t0_exit
t0_ledoff:		cbi		LED_port, LED_pin				
				; odpo�et po�tu bliknut�
				dec		led_counter
				brne	t0_exit
				; doblik�no - vypnut� �asova�e
				rcall	resetLED
t0_exit:		out		SREG, r2
				reti				
;=====================================================================================================			
; Konstanty v FLASH
;=====================================================================================================			
; tabulka posloupnosti clonek
seq_table:		.db		(0 << X1_pin) | (0 << X2_pin), (1 << X1_pin) | (0 << X2_pin)
				.db		(1 << X1_pin) | (1 << X2_pin), (0 << X1_pin) | (1 << X2_pin)

				.db		(0 << Y1_pin) | (0 << Y2_pin), (0 << Y1_pin) | (1 << Y2_pin)
				.db		(1 << Y1_pin) | (1 << Y2_pin), (1 << Y1_pin) | (0 << Y2_pin)
