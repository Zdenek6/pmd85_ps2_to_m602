# PMD85_PS2_TO_M602

Převodník PS/2 myši na emulaci Myš 602 pro počítač PMD85.

Základem je čip ATTiny2313, ke kterému se připojuje PS/2 myš (otestováno s Genius DX-110 a Logitech M-SBF96). Na svém výstupu emuluje Myš 602, tedy dává přímé signály z clonek pro horizontální a vertikální pohyb + emulace stisku levého a pravého tlačítka.

Výstup z převodníku reflektuje i rychlost pohybu myši, tj. pomalejší pohyb generuje delší impulsy (nejen menší počet kratších impulsů).

<b>Schéma zapojení</b>

![image-2.png](./image-2.png)


<b>3D model</b>

![image-1.png](./image-1.png)

Více zde: [https://maximalne.8u.cz/ps2-m602/](https://maximalne.8u.cz/ps2-m602/)

